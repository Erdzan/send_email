require_once 'Clothes.php';

class Shirts extends Clothes 
{
	public $sleeves;

	public function __construct($boja, $velicina, $rakavi)
	{
		parent::__construct($boja, $velicina);
		$this->sleeves = $rakavi;
	}

	public function print()
	{
		echo "Boja $this->color, Velicina $this->size, Rakavi $this->sleeves <br>";
	}
}