<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Http\Request\MovieRequest;

class UserController extends Controller
{
	public function ShowForm()
	{
		return view('form');
	}

	// public function submitForm(MovieRequest $request)
	// {	
	
	// }

	public function submitForm(Request $request)
	{
		$data = $request->all();

		$rules = [
					'username'=> 'required',
					'email'=>	'required',
					'date'=> 'required',
					'password'=> 'required'
		];

		$validator = \Validator::make($data,$rules);


		if ($validator->fails())
		 {
			return redirect()->back()->withErrors($validator);
		 }
		else
		{
			echo 'Valid information';
		}
	}
	
}