<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class MovieRequest extends FormRequest
{
	public function rules()
	{
		$rules = [
					'username'=> 'required',
					'email'=>	'required',
					'date'=> 'required',
					'password'=> 'required'
		];
		return $rules;
	}
	public function authorize()
	{
		return true;
	}
	public function messages()
	{
		$arr=[
				'username.required'=> 'Opps! Korisnikot mora da ima specijalen karakter',
				'email.required'=> 'Email-ot mora da e vo validen format',
				'date.required' => 'korisnikot mora da e postar od 18 godini',
				'password.required' => 'Lozinkata da ima najmalku 8 karakteri i edna golema bukva'
		];

		return $arr;
	}
}

